fun main(args: Array<String>) {
    // driver code here
}

// Task 1
fun isPalindrome(text: String): Boolean {
    return if (text.isEmpty() || text.length == 1)
        true

    else if (text.first() == text.last())
        isPalindrome(text.substring(1, text.length - 1))

    else
        false
}

// Task 2
fun minSplit(amount: Int): Int {
    var minNumCoins = 0
    var total = amount

    while (total > 0) {
        when {
            total >= 50 -> total -= 50
            total >= 20 -> total -= 20
            total >= 10 -> total -= 10
            total >= 5 -> total -= 5
            total >= 1 -> total -= 1
        }

        minNumCoins++
    }
    return minNumCoins
}

// Task 3
fun notContains(array: Array<Int>): Int {
    val minNumber = array.minOrNull() ?: return 1

    return if (minNumber > 1)
        1
    else {
        var num = 1

        while (array.contains(num))
            num++

        num
    }
}

// Task 4
fun isProperly(sequence: String): Boolean {
    var countOpeningBracket = 0
    var countClosingBracket = 0

    for (i in sequence) {
        if (countOpeningBracket < countClosingBracket)
            return false

        when (i) {
            '(' -> countOpeningBracket++
            ')' -> countClosingBracket++
        }
    }

    if (countOpeningBracket != countClosingBracket)
        return false

    return true
}

// Task 5
fun countVariants(stairsCount: Int): Int {
    return if (stairsCount == 1 || stairsCount == 0)
        1
    else if (stairsCount == 2)
        2
    else
        countVariants(stairsCount - 2) + countVariants(stairsCount - 1)
}

// Task 6
class MyStack<E> {

    private var top: Node<E>? = null

    class Node<T>(var data: T) {
        var previous: Node<T>? = null
        var next: Node<T>? = null
    }

    fun push(item: E) {
        if (top == null)
            top = Node(item)
        else {
            val element = Node(item)
            top!!.next = element
            element.previous = top
            top = element
        }
    }

    // the requested O(1) remove operation
    fun pop(): E? {
        return if (top == null)
            null
        else {
            val popped = top

            top!!.previous?.next = null
            top = top!!.previous

            popped!!.data
        }
    }
}











